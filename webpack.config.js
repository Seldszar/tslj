var path = require('path');
var webpack = require('webpack');
var autoprefixer = require('autoprefixer');
// var HtmlWebpackPlugin = require('html-webpack-plugin');


module.exports = {
  entry: './src/main.js',

  output: {
    path: path.resolve(__dirname, './dist'),
    // publicPath: '/dist/',
    filename: 'build.js',
  },

  resolveLoader: {
    root: path.resolve(__dirname, './node_modules'),
  },

  sassLoader: {
    includePaths: [
      path.resolve(__dirname, './src/assets/scss'),
      path.resolve(__dirname, './node_modules/foundation-sites/scss'),
    ],
  },

  module: {
    loaders: [
      {
        test: /\.vue$/,
        loader: 'vue',
      },

      {
        test: /\.js$/,
        loader: 'babel',
        exclude: /node_modules/,
      },

      {
        test: /\.(sass|scss)$/,
        loader: 'style!css!sass',
      },

      {
        test: /\.(png|jpg|gif|svg)$/,
        loader: 'file',
        query: {
          name: './assets/[name].[ext]?[hash]',
        },
      },

      {
        test: /\.(eot|svg|ttf|woff|woff2)$/,
        loader: 'file',
        query: {
          name: './assets/[name].[ext]?[hash]',
        },
      },

      {
        test: /\.(webm|mp3)$/,
        loader: 'file',
        query: {
          name: './assets/[name].[ext]?[hash]',
        },
      },
    ],
  },

  plugins: [
    new webpack.LoaderOptionsPlugin({
      vue: {
        loaders: {
          scss: 'vue-style!css!sass',
        },

        postcss: [
          autoprefixer({
            browsers: ['last 2 versions', 'ie >= 9'],
          }),
        ],
      },
    }),

    // new HtmlWebpackPlugin(),
  ],

  devServer: {
    historyApiFallback: true,
    noInfo: true,
  },

  devtool: '#eval-source-map',
}

if (process.env.NODE_ENV === 'production') {
  module.exports.devtool = '#source-map';

  module.exports.plugins = (module.exports.plugins || []).concat([
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: '"production"',
      },
    }),

    new webpack.optimize.UglifyJsPlugin({
      compress: {
        warnings: false,
      },
    }),
  ]);
}
